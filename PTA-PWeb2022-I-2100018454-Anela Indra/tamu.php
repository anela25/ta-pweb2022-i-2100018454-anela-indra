<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Web Profile</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
</head>
<body>
    <!--sidebar-->
    <input type="checkbox" id="check">
    <div class="sidebar">
        <ul>
            <!--menu-->
            <li><a href="#about">About</a></li>
            <li><a href="#skills">My Skills</a></li>
            <li><a href="#portofolio">Portofolio</a></li>
            <li><a href="#contact">Contact</a></li>
            <li><a href="#bukutamu">Buku Tamu</a></li>
        </ul>
    </div>
    
    <!--header-->
    <header>
    <div class="container">
        <h1><a href="">ANELA INDRA<span></span></h1>
        <ul>
            <li><a href="#about">About</a></li>
            <li><a href="#skills">My Skills</a></li>
            <li><a href="#portofolio">Portofolio</a></li>
            <li><a href="#contact">Contact</a></li>
            <li><a href="#bukutamu">Buku Tamu</a></li>
        </ul>

    <!--menuu-->
    <label for="check" class="mobile-menu"><i class="fas fa-bars fa-2x"></i></label>
    </div>
    </header>

	<!--buku-tamu-->
	<section id="bukutamu">
	<div class="container" align="center">
        <?php
            $name_pesan= $email_pesan= $pesan_pesan= "";
            $name = $email= $pesan= "";

            if ($_SERVER["REQUEST_METHOD"] == "POST")
            {
                if (empty($_POST["name"]))
                {
                    $name_pesan = "Nama harus diisi";
                }
                else
                {
                    $name = htmlspecialchars($_POST["name"]);
                }
                if (empty($_POST["email"]))
                {
                    $email_pesan = "Email harus diisi";
                }
                else
                {
                    $email = htmlspecialchars($_POST["email"]);
                }
                if (empty($_POST["pesan"]))
                {
                    $pesan_pesan = "Pesan harus diisi";
                }
                else
                {
                    $pesan = htmlspecialchars($_POST["pesan"]);
                }
            }
            ?>
            <h3>BUKU TAMU</h3> 
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                Nama: <input type="text" name="name">
                <span class="error">* <?php echo $name_pesan;?></span>
                <br><br>
                Email: <input type="text" name="email">
                <span class="error">* <?php echo $email_pesan;?></span>
                <br><br>
                Pesan: <input type="text" name="pesan">
                <span class="error">* <?php echo $pesan_pesan;?></span>
                <br><br>
                <input type="submit" name="submit" value="Submit"><br><br>
            </form>

            <?php
            echo "<h2>Buku Tamu:</h2>";
            echo "Nama : ".$name;
            echo "<br>";
            echo "E-Mail : ".$email;
            echo "<br>";
            echo "Pesan : ".$pesan;
            ?>
            <br><br>
            <?php
                $data = [
                        "Terima ", "Kasih! ",
                ];
                 
                foreach ($data as $item)
                {
                    echo "$item";
                }
                ?>
        </div>
        </section>