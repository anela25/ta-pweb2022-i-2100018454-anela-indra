<?php
	$arrNilai=array("Anela"=>80, "Husein"=>90, "Vio"=>75, "Ilham"=>85, "Dian"=>89, "Nisa"=>78);
	echo "<b>Array sebelum diurutkan</b>";
	echo "<pre>";
	print_r($arrNilai);
	echo "</pre>";

	asort($arrNilai);
	reset($arrNilai);
	echo "<b>Array setelah diurutkan dengan asort()</b>";
	echo "<pre>";
	print_r($arrNilai);
	echo "</pre>";

	arsort($arrNilai);
	reset($arrNilai);
	echo "<b>Array setelah diurutkan dengan arsort()</b>";
	echo "<pre>";
	print_r($arrNilai);
	echo "</pre>";
?>